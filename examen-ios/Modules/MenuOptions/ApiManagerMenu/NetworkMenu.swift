//
//  NetworkMenu.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import Foundation

class NetworkMenu {
    static let shared: NetworkMenu = NetworkMenu()
    
    func callLogout(serviceURL: String, completion: @escaping (responseData) -> Void) -> Void {
        NetworkManager.shared.dataDeleteTask(serviceURL: serviceURL) { (data, response, error) in
            completion((data, response, error))
        }
    }
}
    
