//
//  MenuOptionsViewController.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import UIKit

class MenuOptionsViewController: UIViewController {
    var presenter: MenuOptionsPresenterProtocol?
    let imageViewLogo: UIImageView = {
        let catImage = UIImage(named: "popcorn")
        let myImageView:UIImageView = UIImageView()
        myImageView.image = catImage
        return myImageView
    }()
    lazy var closedButton: UIButton = {
        let button: UIButton = UIButton()
        let image: UIImage = UIImage(named: Images.closed) ?? UIImage()
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(closed), for: .touchDown)
        return button
    }()
    let stackView: UIStackView = {
        let stackView: UIStackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func commonInit() {
        addSubViews()
        addConstraints()
        self.view.backgroundColor = UIColor.init(named: Colors.BackgroundColor)
    }
    
    private func addSubViews() {
        self.view.addSubview(closedButton)
        self.view.addSubview(imageViewLogo)
        self.view.addSubview(stackView)
        stackView.addArrangedSubview(SelectButtonView(title: "Ver perfil",
                                                      completion: { [weak self] in
                                                        self?.presenter?.showPerfil()
                                                      }))
        stackView.addArrangedSubview(SelectButtonView(title: "Cerrar sesión",
                                                      completion: { [weak self] in
                                                        self?.presenter?.logout()
                                                      }))
    }
    
    private func addConstraints() {
        addConstraintsClosedButton()
        addConstraintsImageView()
        addConstraintsStackView()
    }
    
    private func addConstraintsClosedButton() {
        closedButton.setAnchors(topAnchor: self.view.topAnchor, leadingAnchor: self.view.leadingAnchor, topConstant: 30, leadingConstant: 20)
        closedButton.setHeight(heightConstant: 30)
        closedButton.setWidth(widthConstant: 30)
    }
    
    private func addConstraintsImageView() {
        imageViewLogo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageViewLogo.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            imageViewLogo.widthAnchor.constraint(equalToConstant: 60),
            imageViewLogo.heightAnchor.constraint(equalToConstant: 60),
            imageViewLogo.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    private func addConstraintsStackView() {
        stackView.setAnchors(topAnchor: imageViewLogo.bottomAnchor, leadingAnchor: self.view.leadingAnchor, trailingAnchor: self.view.trailingAnchor, topConstant: 30, leadingConstant: 16, trailingConstant: -16)
    }
    
    @objc private func closed() {
        presenter?.closeMenu()
    }
    
    @objc private func navigateInfoPerfil() {
        presenter?.showPerfil()
    }
}

extension MenuOptionsViewController: MenuOptionsPresenterToViewProtocol {

    
    
}
