//
//  MenuOptionsPresenter.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import Foundation

class MenuOptionsPresenter {
    var view: MenuOptionsPresenterToViewProtocol?
    var interactor: MenuOptionsInteractorProtocol?
    var router: MenuOptionsRouterProtocol?
}

extension MenuOptionsPresenter: MenuOptionsPresenterProtocol {

    func viewDidLoad() {
    }
    
    func closeMenu() {
        router?.closeMenu()
    }
    
    func logout() {
        interactor?.logout()
    }
    
    func showPerfil() {
        router?.navigateToPerfil()
    }
}

extension MenuOptionsPresenter: MenuOptionsInteractorToPresenterProtocol {
    func logoutSuccess() {
        router?.navigateToLogin()
    }
    
    func logoutFailure() {
        print("Fallo cerrar sesión")
    }
    

}
