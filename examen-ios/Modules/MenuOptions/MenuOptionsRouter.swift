//
//  MenuOptionsRouter.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import Foundation

class MenuOptionsRouter: MenuOptionsRouterProtocol {
    
    static func createMenuOptionsViewController() -> MenuOptionsViewController {
        let menuOptionsViewController: MenuOptionsViewController = MenuOptionsViewController()
        let presenter: MenuOptionsPresenterProtocol & MenuOptionsInteractorToPresenterProtocol = MenuOptionsPresenter()
        let interactor: MenuOptionsInteractorProtocol = MenuOptionsInteractor()
        let router: MenuOptionsRouterProtocol = MenuOptionsRouter()
        menuOptionsViewController.presenter = presenter
        presenter.view = menuOptionsViewController
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return menuOptionsViewController
    }
    
    func closeMenu() {
        Router.popViewController(animated: true)
    }
    
    func navigateToLogin() {
        Router.pushViewController(LoginRouter.createLoginViewController(), animated: true)
    }
    
    func navigateToPerfil() {
        Router.pushViewController(ProfileRouter.createProfileViewController(), animated: true)
    }
}
