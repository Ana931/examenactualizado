//
//  MenuOptionsInteractor.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import Foundation

class MenuOptionsInteractor: MenuOptionsInteractorProtocol {
    var presenter: MenuOptionsInteractorToPresenterProtocol?
    
    func logout() {
        ProviderNetworkMenuOptions.shared.logout() { [weak self] (data, response, error) in
            if response != nil {
                self?.logoutSuccessHandler()
            }
            if error != nil {
                self?.presenter?.logoutFailure()
            }
        }
    }
    
    private func logoutSuccessHandler() {
        DispatchQueue.main.async { [weak self] in
            self?.presenter?.logoutSuccess()
        }
    }
}
