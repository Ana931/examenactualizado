//
//  MenuOptionsContract.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import Foundation

protocol MenuOptionsPresenterProtocol: AnyObject {
    var view: MenuOptionsPresenterToViewProtocol? {get set}
    var interactor: MenuOptionsInteractorProtocol? {get set}
    var router: MenuOptionsRouterProtocol? {get set}
    
    func viewDidLoad()
    func closeMenu()
    func showPerfil()
    func logout()
}

protocol MenuOptionsInteractorToPresenterProtocol: AnyObject {
    func logoutSuccess()
    func logoutFailure()
}

protocol MenuOptionsInteractorProtocol: AnyObject {
    var presenter:MenuOptionsInteractorToPresenterProtocol? {get set}
    func logout()
    
}

protocol MenuOptionsPresenterToViewProtocol: AnyObject {
    //func listMovieSuccess(listMovie: ListMovieModels)
    //func listMovieFailure()
}

protocol MenuOptionsRouterProtocol: AnyObject {
    static func createMenuOptionsViewController() -> MenuOptionsViewController
    func closeMenu()
    func navigateToLogin()
    func navigateToPerfil()
}
