//
//  SplashViewController.swift
//  examen-ios
//
//  Created by User on 27/09/22.
//

import Foundation
import Lottie
import UIKit

class SplashViewController: UIViewController {
    
    private var lottieAnimation: AnimationView?
    
    override func viewDidLoad() {
      super.viewDidLoad()
      self.view.backgroundColor = .white
      Router(self.navigationController)
      playAnimation()
      hidenSplash()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func playAnimation() {
        self.lottieAnimation = AnimationView(name: "screen")
        self.lottieAnimation?.frame = view.bounds
        self.lottieAnimation?.contentMode = .scaleAspectFill
        self.lottieAnimation?.frame = CGRect(x: 0, y: 0, width: 200, height: 300)
        self.lottieAnimation?.center = self.view.center
        self.view.addSubview(self.lottieAnimation!)
        self.lottieAnimation?.loopMode = .loop
        self.lottieAnimation?.play()
    }
    
    func hidenSplash() {
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: false)
    }
    
    @objc func fireTimer() {
        DispatchQueue.main.async {
            if CoreDataManager.shared.fetchData() != "" {
                let listMovieViewController: ListMovieViewController = ListMovieRouter.createListMovieViewController()
                Router.pushViewController(listMovieViewController, animated: true)
            } else {
                let loginViewControler = LoginRouter.createLoginViewController()
                Router.pushViewController(loginViewControler, animated: true)
            }
        }
       
    }
}
