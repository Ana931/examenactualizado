//
//  DetailMovieContract.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import Foundation

protocol DetailMoviePresenterProtocol: AnyObject {
    var view: DetailMoviePresenterToViewProtocol? {get set}
    var interactor: DetailMovieInteractorProtocol? {get set}
    var router: DetailMovieRouterProtocol? {get set}
    
    func viewDidLoad()
    func callListMovie(actionType: ActionType)
}

protocol DetailMovieInteractorToPresenterProtocol: AnyObject {
    func listMovieSuccess(listMovie: ListMovieModels)
    func listMovieFailure()
}

protocol DetailMovieInteractorProtocol: AnyObject {
    var presenter: DetailMovieInteractorToPresenterProtocol? {get set}
    func callListMovie(filterType: ActionType)
    
}

protocol DetailMoviePresenterToViewProtocol: AnyObject {
    init(value: MovieModels)
    func listMovieSuccess(listMovie: ListMovieModels)
    func listMovieFailure()
}

protocol DetailMovieRouterProtocol: AnyObject {
    static func createDetailMovieViewController(value: MovieModels) -> DetailMovieViewController
}
