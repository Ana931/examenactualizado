//
//  DetailMovieRouter.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import Foundation

class DetailMovieRouter: DetailMovieRouterProtocol {
    static func createDetailMovieViewController(value: MovieModels) -> DetailMovieViewController {
        let listMovieViewController: DetailMovieViewController = DetailMovieViewController(value: value)
        let presenter: DetailMoviePresenterProtocol & DetailMovieInteractorToPresenterProtocol = DetailMoviePresenter()
        let interactor: DetailMovieInteractorProtocol = DetailMovieInteractor()
        let router: DetailMovieRouterProtocol = DetailMovieRouter()
        listMovieViewController.presenter = presenter
        presenter.view = listMovieViewController
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return listMovieViewController
    }
}
