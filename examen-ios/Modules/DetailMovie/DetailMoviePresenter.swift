//
//  DetailMoviePresenter.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import Foundation

class DetailMoviePresenter {
    var view: DetailMoviePresenterToViewProtocol?
    var interactor: DetailMovieInteractorProtocol?
    var router: DetailMovieRouterProtocol?
}

extension DetailMoviePresenter: DetailMoviePresenterProtocol {
    func viewDidLoad() {
        
    }
    
    func callListMovie(actionType: ActionType) {
        
    }
    
}

extension DetailMoviePresenter: DetailMovieInteractorToPresenterProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        
    }
    
    func listMovieFailure() {
        
    }
    
    
}
