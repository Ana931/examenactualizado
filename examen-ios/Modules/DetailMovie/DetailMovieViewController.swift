//
//  DetailMovieViewController.swift
//  examen-ios
//
//  Created by User on 01/11/22.
//

import UIKit

class DetailMovieViewController: BaseViewController {
    var presenter: DetailMoviePresenterProtocol?
    
    private lazy var pictureMovie: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named: Images.movieTadeo)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    private lazy var titleMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    private lazy var dateMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    private lazy var pointMovieMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    private lazy var descriptionShortMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var addFavoriteButton: UIButton = {
        let button: UIButton = UIButton()
        button.setTitle("Agregar a favoritos ♡♡♥♥", for: .normal)
        return button
    }()
    
    private enum LayoutConstant {
        static let spacing: CGFloat = 8.0
        static let itemHeight: CGFloat = 250.0
    }
    
    private var value: MovieModels?
    
    required init(value: MovieModels) {
        super.init(nibName: nil, bundle: nil)
        self.value = value
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        commontInit()
        guard let value: MovieModels = self.value else {
            return
        }
        dataContent(with: value)
    }
    
    private func commontInit() {
        addSubViews()
        addConstraints()
        addTargets()
        view.backgroundColor = UIColor.init(named: Colors.Gray400)
    }
    
    private func addSubViews() {
        self.view.addSubview(pictureMovie)
        self.view.addSubview(titleMovieLabel)
        self.view.addSubview(dateMovieLabel)
        self.view.addSubview(pointMovieMovieLabel)
        self.view.addSubview(descriptionShortMovieLabel)
        self.view.addSubview(addFavoriteButton)
    }
    
    private func addConstraints() {
        addConstraintsForPictureMovie()
        addConstraintsForTitleMovieLabel()
        addConstraintsForDateMovieLabel()
        addConstraintsForPointMovieMovieLabel()
        addConstraintForDescriptionShortMovieLabel()
        addContraintForAddMoreButton()
    }
    
    private func addConstraintsForPictureMovie() {
        pictureMovie.setAnchors(topAnchor: view.topAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor, leadingConstant: -5, trailingConstant: 5)
        pictureMovie.setHeight(heightConstant: 60)
    }
    
    private func addConstraintsForTitleMovieLabel() {
        titleMovieLabel.setAnchors(topAnchor: pictureMovie.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor, topConstant: 110)
        titleMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addConstraintsForDateMovieLabel() {
        dateMovieLabel.setAnchors(topAnchor: titleMovieLabel.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor,topConstant: 2)
        dateMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addConstraintsForPointMovieMovieLabel() {
        pointMovieMovieLabel.setAnchors(topAnchor: dateMovieLabel.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor)
        pointMovieMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addConstraintForDescriptionShortMovieLabel() {
        descriptionShortMovieLabel.setAnchors(topAnchor: pointMovieMovieLabel.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor)
        descriptionShortMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addContraintForAddMoreButton() {
        addFavoriteButton.setAnchors(topAnchor: descriptionShortMovieLabel.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor)
    }
    
    func dataContent(with dataContent: MovieModels) {
        titleMovieLabel.text = dataContent.firstAirDate
        dateMovieLabel.text = dataContent.name
        pointMovieMovieLabel.text = "\(dataContent.voteCount)"
        descriptionShortMovieLabel.text = dataContent.overview
    }
    
    private func addTargets() {
        addFavoriteButton.addTarget(self, action: #selector(addFavoriteAction), for: .touchDown)
    }
    
    @objc private func addFavoriteAction() {
        guard let value: MovieModels = self.value else {
            return
        }
        MoviesFavoriteFrom.array.append(value)
    }
}

extension DetailMovieViewController: DetailMoviePresenterToViewProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        
    }
    
    func listMovieFailure() {
        
    }
    
}
