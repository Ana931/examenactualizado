//
//  ProviderNetworkLogin.swift
//  examen-ios
//
//  Created by User on 28/09/22.
//


import UIKit

class ProviderNetworkLogin {

    static let shared: ProviderNetworkLogin = ProviderNetworkLogin()
    func callLogin(userText: String, password: String, completion: @escaping (responseData) -> Void) -> Void {
        let parameters: parametersBody = ["username": userText, "password": password,"request_token": KeyFromDataImplement.shared.token ?? ""]
        NetworkLogin.shared.callLogin(serviceURL: "3/authentication/token/validate_with_login?api_key=\(ApiKey.data)", parameters: parameters) { (data, response, error) in
            completion((data, response, error))
        }
    }
    
    func getToken() {
        NetworkLogin.shared.getToken(serviceURL:  "3/authentication/token/new?api_key=\(ApiKey.data)") { (data, response, error) in
            if response != nil {
                let decoder = JSONDecoder()
                if let data: Data = data {
                    if let jsonPetitions: GetTokenModel = try? decoder.decode(GetTokenModel.self, from: data) {
                        KeyFromDataImplement.shared.token = jsonPetitions.requestToken
                    }
                } else {
                }
                
                debugPrint(response)
            }
            if error != nil {
                print("ERRORRRR")
                print(error)
            }
        }
    }
}


