//
//  LoginPresenter.swift
//  examen-ios
//
//  Created by User on 18/09/22.
//

import Foundation

class LoginPresenter {
    var view: LoginPresenterToViewProtocol?
    var interactor: LoginInteractorProtocol?
    var router: LoginRouterProtocol?
}

extension LoginPresenter: LoginPresenterProtocol {
    
    func viewDidLoad() {
        interactor?.getToken()
    }
    
    func loginNextNavigation(userText: String, password: String) {
        print("entro al presenter")
        interactor?.callLogin(userText: userText, password: password)
    }
}

extension LoginPresenter: LoginInteractorToPresenterProtocol {
    func loginSuccess() {
        router?.navigateToMainApp()
    }
    
    func loginFailure() {
        view?.loginFailure()
    }
}
