//
//  LoginViewController.swift
//  examen-ios
//
//  Created by User on 18/09/22.
//

import Foundation
import UIKit

class LoginViewController: BaseLoginViewController {
    var presenter: LoginPresenterProtocol?
    let imageViewLogo: UIImageView = {
        let catImage = UIImage(named: "popcorn")
        let myImageView:UIImageView = UIImageView()
        myImageView.image = catImage
        return myImageView
    }()
    let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 30)
        label.text = "Sign In"
        label.textAlignment = .left
        return label
    }()
    let userLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Poppins-Bold", size: 20)
        label.text = "Username"
        label.textAlignment = .left
        return label
    }()
    let userTexField: TextField = {
        let textfield: TextField = TextField()
        return textfield
    }()
    let passwordLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Poppins-Bold", size: 20)
        label.text = "Password"
        label.textAlignment = .left
        return label
    }()
    let passwordTexField: TextField = {
        let textfield: TextField = TextField()
        return textfield
    }()
    let loginButton: Button = {
        let button: Button = Button(title: "Log In", typeButton: .primary)
        return button
    }()
    let errorLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .red
        label.font = UIFont(name: "Poppins-Bold", size: 20)
        label.textAlignment = .left
        return label
    }()
    let scrollView: UIScrollView = {
        let scrollView: UIScrollView = UIScrollView()
        return scrollView
    }()
    
    private enum Margin {
        static let trailing: CGFloat = -30
        static let leading: CGFloat = 30
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        setupTextFields()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func commonInit() {
        addSubViews()
        addConstraints()
        registerForKeyboardNotifications()
        addTargets()
    }
    
    private func addSubViews() {
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(imageViewLogo)
        self.scrollView.addSubview(titleLabel)
        self.scrollView.addSubview(userLabel)
        self.scrollView.addSubview(userTexField)
        self.scrollView.addSubview(passwordLabel)
        self.scrollView.addSubview(passwordTexField)
        self.scrollView.addSubview(loginButton)
        self.scrollView.addSubview(errorLabel)
    }
    
    private func addConstraints() {
        addConstraintForScrollView()
        addConstraintsImageView()
        addConstraintsTitleLabel()
        addConstraintsUserLabel()
        addConstraintsForUserTexField()
        addConstraintsPasswordLabel()
        addConstraintsForPasswordTexField()
        addConstraintForLoginButton()
        addConstraintForLabelError()
    }
    
    private func addConstraintForScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.setAnchors(topAnchor: self.view.topAnchor, bottomAnchor: self.view.bottomAnchor, leadingAnchor: self.view.leadingAnchor, trailingAnchor: self.view.trailingAnchor)
    }
    
    private func addConstraintsImageView() {
        imageViewLogo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageViewLogo.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 60),
            imageViewLogo.widthAnchor.constraint(equalToConstant: 120),
            imageViewLogo.heightAnchor.constraint(equalToConstant: 120),
            imageViewLogo.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
        ])
    }
    
    private func addConstraintsTitleLabel() {
        titleLabel.setAnchors(topAnchor: imageViewLogo.bottomAnchor,
                              leadingAnchor: self.scrollView.leadingAnchor,
                              topConstant: 40,
                              leadingConstant: Margin.leading)
    }
    
    private func addConstraintsUserLabel() {
        userLabel.setAnchors(topAnchor: titleLabel.bottomAnchor,
                             leadingAnchor: self.scrollView.leadingAnchor,
                             topConstant: 20,
                             leadingConstant: Margin.leading)
    }
    private func addConstraintsForUserTexField() {
        userTexField.setAnchors(topAnchor: userLabel.bottomAnchor,
                                leadingAnchor: self.scrollView.leadingAnchor,
                                trailingAnchor: self.view.trailingAnchor,
                                topConstant: 10,
                                leadingConstant: Margin.leading,
                                trailingConstant: Margin.trailing)
        userTexField.setHeight(heightConstant: 50)
    }
    
    private func addConstraintsPasswordLabel() {
        passwordLabel.setAnchors(topAnchor: userTexField.bottomAnchor,
                                 leadingAnchor: self.scrollView.leadingAnchor,
                                 trailingAnchor: self.view.trailingAnchor,
                                 topConstant: 20,
                                 leadingConstant: Margin.leading)
    }
    
    private func addConstraintsForPasswordTexField() {
        passwordTexField.setAnchors(topAnchor: passwordLabel.bottomAnchor,
                                    leadingAnchor: self.scrollView.leadingAnchor,
                                    trailingAnchor: self.view.trailingAnchor,
                                    topConstant: 10,
                                    leadingConstant: Margin.leading,
                                    trailingConstant: Margin.trailing)
        passwordTexField.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForLoginButton() {
        loginButton.setAnchors(topAnchor: passwordTexField.bottomAnchor,
                               leadingAnchor: self.scrollView.leadingAnchor,
                               trailingAnchor: self.view.trailingAnchor,
                               topConstant: 50,
                               leadingConstant: Margin.leading,
                               trailingConstant: Margin.trailing)
        loginButton.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForLabelError() {
        errorLabel.setAnchors(topAnchor: loginButton.bottomAnchor,
                              leadingAnchor: self.scrollView.leadingAnchor,
                              trailingAnchor: self.view.trailingAnchor,
                              topConstant: 25,
                              leadingConstant: Margin.leading,
                              trailingConstant: Margin.trailing)
    }
    
    private func addTargets() {
        loginButton.addTarget(self, action: #selector(loginNext), for: .touchDown)
    }
    
    @objc func loginNext() {
        guard let userText: String = userTexField.text else {
            print("Error user text")
            showAlert(message: "Necesita agregar el usuario")
            return
        }
        guard let passwordText: String = passwordTexField.text else {
            print("Error password text")
            showAlert(message: "Necesita agregar la contraseña")
            return
        }
        print(userText)
        print(passwordText)
        presenter?.loginNextNavigation(userText: userText, password: passwordText)
    }
    
    func setupTextFields() {
        let toolbar: UIToolbar = UIToolbar()
        let flexSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneButton: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done,
                                             target: self, action: #selector(doneButtonTapped))
            
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        userTexField.inputAccessoryView = toolbar
        passwordTexField.inputAccessoryView = toolbar
    }
        
    @objc func doneButtonTapped() {
        view.endEditing(true)
    }
    
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
   }

   func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
   }

    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}

extension LoginViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension LoginViewController: LoginPresenterToViewProtocol {
    func loginFailure() {
        print("llego a view")
        showAlert(message: "Usuario o contraseña no validas")
    }
}
