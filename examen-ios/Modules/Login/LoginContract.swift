//
//  LoginContract.swift
//  examen-ios
//
//  Created by User on 18/09/22.
//

import Foundation

protocol LoginPresenterProtocol: AnyObject {
    var view: LoginPresenterToViewProtocol? {get set}
    var interactor: LoginInteractorProtocol? {get set}
    var router: LoginRouterProtocol? {get set}
    
    func viewDidLoad()
    func loginNextNavigation(userText: String, password: String)
}

protocol LoginInteractorToPresenterProtocol: AnyObject {
    func loginSuccess()
    func loginFailure()
}

protocol LoginInteractorProtocol: AnyObject {
    var presenter: LoginInteractorToPresenterProtocol? {get set}
    func getToken()
    func callLogin(userText: String, password: String)
    
}

protocol LoginPresenterToViewProtocol: AnyObject {
    func loginFailure()
}

protocol LoginRouterProtocol: AnyObject {
    static func createLoginViewController() -> LoginViewController
    func navigateToMainApp()
}
