//
//  LoginInteractor.swift
//  examen-ios
//
//  Created by User on 18/09/22.
//

import UIKit
import CoreData


class LoginInteractor: LoginInteractorProtocol {
    
    var presenter: LoginInteractorToPresenterProtocol?
    
    func getToken() {
        ProviderNetworkLogin.shared.getToken()
    }
    
    func callLogin(userText: String, password: String) {
        ProviderNetworkLogin.shared.callLogin(userText: userText, password: password) { [weak self] (data, response, error) in
            if response != nil {
                let decoder = JSONDecoder()
                if let data: Data = data {
                    if let jsonPetitions: LoginModel = try? decoder.decode(LoginModel.self, from: data) {
                        CoreDataManager.shared.saveData()
                        CoreDataManager.shared.fetchData()
                        self?.successHandler()
                    } else {
                        self?.presenter?.loginFailure()
                    }
                } else {
                    self?.presenter?.loginFailure()
                    print("fail")
                }
            }
            if error != nil {
                print("fail")
                self?.presenter?.loginFailure()
            }
        }
    }
    
    private func successHandler() {
        DispatchQueue.main.async { [weak self] in
            self?.presenter?.loginSuccess()
        }
        
    }
    
   
}











