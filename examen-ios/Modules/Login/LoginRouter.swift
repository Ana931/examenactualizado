//
//  LoginRouter.swift
//  examen-ios
//
//  Created by User on 18/09/22.
//

import Foundation

class LoginRouter: LoginRouterProtocol {
    static func createLoginViewController() -> LoginViewController {
        let loginViewController: LoginViewController = LoginViewController()
        let presenter: LoginPresenterProtocol & LoginInteractorToPresenterProtocol = LoginPresenter()
        let interactor: LoginInteractorProtocol = LoginInteractor()
        let router: LoginRouterProtocol = LoginRouter()
        loginViewController.presenter = presenter
        presenter.view = loginViewController
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return loginViewController
    }
    
    func navigateToMainApp() {
        Router.pushViewController(ListMovieRouter.createListMovieViewController(), animated: true)
    }
}
