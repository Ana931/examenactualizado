//
//  LoginModel.swift
//  examen-ios
//
//  Created by User on 28/09/22.
//

import Foundation

struct LoginModel: Codable {
    var success: Bool
    var expiresAt: String
    var requestToken: String
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}
