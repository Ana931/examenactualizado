//
//  ProfilePresenter.swift
//  examen-ios
//
//  Created by User on 24/10/22.
//

import Foundation

class ProfilePresenter {
    var view: ProfilePresenterToViewProtocol?
    var interactor: ProfileInteractorProtocol?
    var router: ProfileRouterProtocol?
}

extension ProfilePresenter: ProfilePresenterProtocol {

    func viewDidLoad() {
        interactor?.callListMovie()
    }
    
    func closeMenu() {
        router?.closeMenu()
    }
}

extension ProfilePresenter: ProfileInteractorToPresenterProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        view?.listMovieSuccess(listMovie: listMovie)
    }
    
    func listMovieFailure() {
        view?.listMovieFailure()
    }
}
