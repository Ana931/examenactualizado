//
//  ProfileViewController.swift
//  examen-ios
//
//  Created by User on 24/10/22.
//

import UIKit

class ProfileViewController: BaseViewController {
    var profileView: ProfileView = ProfileView()
    var presenter: ProfilePresenterProtocol?
    lazy var closedButton: UIButton = {
        let button: UIButton = UIButton()
        let image: UIImage = UIImage(named: Images.closed) ?? UIImage()
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(closed), for: .touchDown)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commontInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func commontInit() {
        addSubViews()
        addConstraints()
        profileView.galeryMovieCollectionView.results = MoviesFavoriteFrom.array
    }
    
    private func addSubViews() {
        self.view.addSubview(closedButton)
        self.view.addSubview(profileView)
    }
    
    private func addConstraints() {
        addConstraintsForProfileView()
        addConstraintsClosedButton()
    }
    
    private func addConstraintsClosedButton() {
        closedButton.setAnchors(topAnchor: self.view.topAnchor, leadingAnchor: self.view.leadingAnchor, topConstant: 30, leadingConstant: 20)
        closedButton.setHeight(heightConstant: 30)
        closedButton.setWidth(widthConstant: 30)
    }
    
    private func addConstraintsForProfileView() {
        profileView.setAnchors(topAnchor: closedButton.bottomAnchor, bottomAnchor: self.view.bottomAnchor, leadingAnchor: self.view.leadingAnchor, trailingAnchor: self.view.trailingAnchor)
    }
    
    @objc private func closed() {
        presenter?.closeMenu()
    }
}

extension ProfileViewController: ProfilePresenterToViewProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        
    }
    
    func listMovieFailure() {
        
    }
}
    
