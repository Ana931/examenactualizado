//
//  ProfileView.swift
//  examen-ios
//
//  Created by User on 24/10/22.
//

import UIKit

class ProfileView: UIView {
    
    var profileTitleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.text = "Perfil"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    var photoProfileImageView: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named: "photoProfileDefault")
        return imageView
    }()
    
    var profileLabel: UILabel = {
        let label: UILabel = UILabel()
        return label
    }()
    
    var favoriteLabel: UILabel = {
        let label: UILabel = UILabel()
        label.text = "Peliculas favoritas"
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    lazy var galeryMovieCollectionView: GaleryMovieCollectionView = {
        let galeryMovieCollectionView: GaleryMovieCollectionView = GaleryMovieCollectionView()
        return galeryMovieCollectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commontInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commontInit() {
        addSubViews()
        addConstraints()
    }
    
    private func addSubViews() {
        self.addSubview(profileTitleLabel)
        self.addSubview(photoProfileImageView)
        self.addSubview(profileLabel)
        self.addSubview(favoriteLabel)
        self.addSubview(galeryMovieCollectionView)
    }
    
    private func addConstraints() {
        addConstraintForProfileTitleLabel()
        addConstraintForPhotoProfileImageView()
        addConstraintForProfileLabel()
        addConstraintForFavoriteLabel()
        addConstraintForGaleryMovieCollectionView()
    }
    
    private func addConstraintForProfileTitleLabel() {
        profileTitleLabel.setAnchors(topAnchor: self.topAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor, topConstant: 30, leadingConstant: 16)
        profileTitleLabel.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForPhotoProfileImageView() {
        photoProfileImageView.setAnchors(topAnchor: profileTitleLabel.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor)
        photoProfileImageView.setHeight(heightConstant: 60)
        photoProfileImageView.setWidth(widthConstant: 60)
        
    }
    
    private func addConstraintForProfileLabel() {
        profileLabel.setAnchors(topAnchor: photoProfileImageView.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor)
        profileLabel.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForFavoriteLabel() {
        favoriteLabel.setAnchors(topAnchor: photoProfileImageView.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor)
        favoriteLabel.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForGaleryMovieCollectionView() {
        galeryMovieCollectionView.setAnchors(topAnchor: favoriteLabel.bottomAnchor, bottomAnchor: self.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor)
    }
    
}
