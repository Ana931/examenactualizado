//
//  ListMovieView.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import UIKit

class ListMovieView: UIView {
    
    var delegate: ListMovieDelegate?
    private lazy var popularButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Gray400)
        button.setTitle("Popular", for: .normal)
        button.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tag = ActionType.popular.rawValue
        return button
    }()
    private lazy var topRatedButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Gray400)
        button.setTitle("Top Rated", for: .normal)
        button.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tag = ActionType.topRate.rawValue
        return button
    }()
    private lazy var onTvButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Gray400)
        button.setTitle("On Tv", for: .normal)
        button.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tag = ActionType.onTv.rawValue
        return button
    }()
    private lazy var airingTodayButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Gray400)
        button.setTitle("Airing", for: .normal)
        button.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tag = ActionType.airing.rawValue
        return button
    }()
    private lazy var buttonsStackView: UIStackView = {
        let stackView: UIStackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        return stackView
    }()
    lazy var galeryMovieCollectionView: GaleryMovieCollectionView = {
        let galeryMovieCollectionView: GaleryMovieCollectionView = GaleryMovieCollectionView()
        return galeryMovieCollectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commontInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commontInit() {
        addSubViews()
        addConstraints()
        addTargets()
    }
    
    private func addSubViews() {
        buttonsStackView.addArrangedSubview(popularButton)
        buttonsStackView.addArrangedSubview(topRatedButton)
        buttonsStackView.addArrangedSubview(onTvButton)
        buttonsStackView.addArrangedSubview(airingTodayButton)
        self.addSubview(buttonsStackView)
        self.addSubview(galeryMovieCollectionView)
    }
    
    private func addConstraints() {
        addConstraintForButtonsStackView()
        addConstraintForGaleryMovieCollectionView()
    }
    
    private func addConstraintForButtonsStackView() {
        buttonsStackView.setAnchors(topAnchor: self.safeAreaLayoutGuide.topAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor, topConstant: 10, leadingConstant: 16, trailingConstant: -16)
        buttonsStackView.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForGaleryMovieCollectionView() {
        galeryMovieCollectionView.setAnchors(topAnchor: buttonsStackView.bottomAnchor, bottomAnchor: self.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor, topConstant: 18, leadingConstant: 16, trailingConstant: -16)
    }
    
    private func addTargets() {
        popularButton.addTarget(self, action: #selector(buttonAction), for: .touchDown)
        topRatedButton.addTarget(self, action: #selector(buttonAction), for: .touchDown)
        onTvButton.addTarget(self, action: #selector(buttonAction), for: .touchDown)
        airingTodayButton.addTarget(self, action: #selector(buttonAction), for: .touchDown)
    }
    
    @objc private func buttonAction(_ sender: UIButton) {
        delegate?.actionFilterMovie(actionType: ActionType(rawValue: sender.tag) ?? .none)
        selectInsertColor(actionType: ActionType(rawValue: sender.tag) ?? .none)
    }
    
    private func selectInsertColor(actionType: ActionType) {
        switch actionType {
        case .popular:
            popularButton.backgroundColor = UIColor.yellow
            topRatedButton.backgroundColor = UIColor.red
            onTvButton.backgroundColor = UIColor.red
            airingTodayButton.backgroundColor = UIColor.red
        case .topRate:
            popularButton.backgroundColor = UIColor.red
            topRatedButton.backgroundColor = UIColor.yellow
            onTvButton.backgroundColor = UIColor.red
            airingTodayButton.backgroundColor = UIColor.red
        case .onTv:
            popularButton.backgroundColor = UIColor.red
            topRatedButton.backgroundColor = UIColor.red
            onTvButton.backgroundColor = UIColor.yellow
            airingTodayButton.backgroundColor = UIColor.red
        case .airing:
            popularButton.backgroundColor = UIColor.red
            topRatedButton.backgroundColor = UIColor.red
            onTvButton.backgroundColor = UIColor.red
            airingTodayButton.backgroundColor = UIColor.yellow
        case .none:
            popularButton.backgroundColor = UIColor.red
            topRatedButton.backgroundColor = UIColor.red
            onTvButton.backgroundColor = UIColor.red
            airingTodayButton.backgroundColor = UIColor.red
        default:
            print("")
        }
        
    }
}

protocol ListMovieDelegate {
    func actionFilterMovie(actionType: ActionType)
}
