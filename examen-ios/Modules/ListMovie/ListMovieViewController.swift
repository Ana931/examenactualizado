//
//  ListMovieViewController.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import UIKit

class ListMovieViewController: BaseViewController {
    
    var listMovieView: ListMovieView = ListMovieView()
    var presenter: ListMoviePresenterProtocol?
    var results: [MovieModels] = []
    private enum LayoutConstant {
        static let spacing: CGFloat = 8.0
        static let itemHeight: CGFloat = 250.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubViews()
        addConstraints()
        presenter?.viewDidLoad()
        listMovieView.delegate = self
        listMovieView.galeryMovieCollectionView.delegate = self
    }
    
    private func addSubViews() {
        self.view.addSubview(listMovieView)
    }
    
    private func addConstraints() {
        listMovieView.setAnchors(topAnchor: self.view.topAnchor, bottomAnchor: self.view.bottomAnchor, leadingAnchor: self.view.leadingAnchor, trailingAnchor: self.view.trailingAnchor)
    }
}

extension ListMovieViewController: ListMoviePresenterToViewProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        self.results = listMovie.results
        listMovieView.galeryMovieCollectionView.results = []
        listMovieView.galeryMovieCollectionView.results = listMovie.results
        DispatchQueue.main.async { [weak self] in
            self?.listMovieView.galeryMovieCollectionView.colletionView.reloadData()
        }
    }
    
    func listMovieFailure() {
        
    }
}

extension ListMovieViewController: ListMovieDelegate {
    func actionFilterMovie(actionType: ActionType) {
        presenter?.callListMovie(actionType: actionType)
    }
}

extension ListMovieViewController: GaleryMovieCollectionDelegate {
    func selectToRow(didSelectItemAt indexPath: IndexPath) {
        let value: MovieModels = results[indexPath.row]
        presenter?.navigateToDetailMovie(value: value)
    }
    
}
