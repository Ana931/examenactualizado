//
//  ListMovieRouter.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class ListMovieRouter: ListMovieRouterProtocol {
    
    static func createListMovieViewController() -> ListMovieViewController {
        let listMovieViewController: ListMovieViewController = ListMovieViewController()
        let presenter: ListMoviePresenterProtocol & ListMovieInteractorToPresenterProtocol = ListMoviePresenter()
        let interactor: ListMovieInteractorProtocol = ListMovieInteractor()
        let router: ListMovieRouterProtocol = ListMovieRouter()
        listMovieViewController.presenter = presenter
        presenter.view = listMovieViewController
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return listMovieViewController
    }
    
    func navigateToDetailMovie(value: MovieModels) {
        Router.presentViewController(DetailMovieRouter.createDetailMovieViewController(value: value), animated: true)
    }
}
