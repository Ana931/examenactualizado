//
//  ListMovieInteractor.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class ListMovieInteractor: ListMovieInteractorProtocol {
    var presenter: ListMovieInteractorToPresenterProtocol?
    
    func callListMovie(filterType: ActionType) {
        ProviderNetworkListMovie.shared.callListMovie(filterType: filterType) { [weak self] (data, response, error) in
            if response != nil {
                let decoder = JSONDecoder()
                if let data: Data = data {
                    if let jsonPetitions: ListMovieModels = try? decoder.decode(ListMovieModels.self, from: data) {
                        self?.presenter?.listMovieSuccess(listMovie: jsonPetitions)
                    }
                } else {
                    self?.presenter?.listMovieFailure()
                }
            }
            if error != nil {
                self?.presenter?.listMovieFailure()
            }
        }
    }
}
