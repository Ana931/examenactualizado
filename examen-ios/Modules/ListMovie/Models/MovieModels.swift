//
//  ListMovieModels.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import UIKit

struct MovieModels: Codable {
    //let backdropPath: String
    let firstAirDate: String
    //let genreIds: [Int]
    let id: Int
    let name: String
    let originCountry: [String]
    let originalLanguage: String
    let originalName: String
    let voteCount: Int
    let overview: String
    let popularity: CGFloat
    //let posterPath: String
    //let voteAverage: Double
    //
    
    enum CodingKeys: String, CodingKey {
        //case backdropPath = "backdrop_path"
        case firstAirDate = "first_air_date"
        //case genreIds = "genre_ids"
        case id = "id"
        case name = "name"
        case originCountry = "origin_country"
        case originalLanguage = "original_language"
        case originalName = "original_name"
        case voteCount = "vote_count"
        case overview = "overview"
        case popularity = "popularity"
        //case posterPath = "poster_path"
        //case voteAverage = "vote_average"
        
     }
}
