//
//  ProviderNetworkListMovie.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class ProviderNetworkListMovie {
    static let shared: ProviderNetworkListMovie = ProviderNetworkListMovie()
    func callListMovie(filterType: ActionType, completion: @escaping (responseData) -> Void) -> Void {
        NetworkListMovie.shared.callListMovie(serviceURL: prepareUrl(filterType: filterType)) { (data, response, error) in
            completion((data, response, error))
        }
    }
    
    private func prepareUrl(filterType: ActionType) -> String {
        var value: String = ""
        switch filterType {
        case .popular:
            print("")
            value = "popular"
        case .topRate:
            print("")
            value = "top_rate"
        case .onTv:
            print("")
            value = "on_tv"
        case .airing:
            print("")
            value = "airing_today"
        case .none:
            print("")
        default:
            print("")
        }
        return "3/tv/\(value)?api_key=\(ApiKey.data)&language=en-US&page=1"
    }
}
