//
//  HttpMethod.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

enum HttpMethod:String{
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}
