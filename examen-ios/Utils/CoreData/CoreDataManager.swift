//
//  CoreDataManager.swift
//  examen-ios
//
//  Created by User on 04/10/22.
//

import UIKit
import CoreData

class CoreDataManager {
    static var shared: CoreDataManager = CoreDataManager()
    
    var context:NSManagedObjectContext!
    let appDelegate = (UIApplication.shared.delegate) as? AppDelegate //Singlton instance
    private var userDBObj: NSManagedObject?
    private func openDatabse() {
        context = appDelegate?.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Key", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        userDBObj = newUser
    }
    
    func saveData() {
        openDatabse()
        userDBObj?.setValue(KeyFromDataImplement.shared.token ?? "",forKey: "token" )
        print("Storing Data..")
        do {
            try context.save()
        } catch {
            print("Storing data Failed")
        }
    }

    func fetchData() -> String {
        openDatabse()
        print("Fetching Data..")
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Key")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let token = data.value(forKey: "token") as! String
                print("Token Save: "+token)
                return token
            }
        } catch {
            return ""
            print("Fetching data Failed")
        }
        return ""
    }
}
