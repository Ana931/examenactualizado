//
//  Button.swift
//  examen-ios
//
//  Created by User on 23/09/22.
//

import UIKit

class Button: UIButton {
    
    public var typeButton: TypeButton = .primary
    
    required init(title: String, typeButton: TypeButton) {
        super.init(frame: .zero)
        self.setTitle(title, for: .normal)
        commonInit(typeButton: typeButton)
    }

    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func commonInit(typeButton: TypeButton) {
        configureView(typeButton: typeButton)
        //self.clipsToBounds = true
        //self.roundCorners(corners: [.allCorners], radius: 8)
    }
    
    func configureView(typeButton: TypeButton) {
        switch typeButton {
        case .primary:
            self.backgroundColor = UIColor.init(named: Colors.Red500)
            self.setTitleColor(UIColor.init(named: Colors.Gray300), for: .normal)
        case .secondary:
            self.backgroundColor = UIColor.init(named: Colors.Gray300)
            self.setTitleColor(UIColor.black, for: .normal)
        }
    }
}


enum TypeButton {
    case primary
    case secondary
}
