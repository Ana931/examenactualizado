//
//  Extension+View.swift
//  examen-ios
//
//  Created by User on 23/09/22.
//

import UIKit

extension UIView {
    
    func addBorderAndColor(color: UIColor, width: CGFloat, cornerRadius: CGFloat = 0, clipsToBounds: Bool = false) {
            self.layer.borderWidth = width
            self.layer.borderColor = color.cgColor
            self.layer.cornerRadius = cornerRadius
            self.clipsToBounds = clipsToBounds
        }
    
}
