//
//  TextField.swift
//  examen-ios
//
//  Created by User on 23/09/22.
//

import UIKit

class TextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commontInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commontInit() {
        //self.clipsToBounds = true
        //self.roundCorners(corners: [.allCorners], radius: 8)
        self.font = UIFont.systemFont(ofSize: 16)
        self.textColor = UIColor.init(named: Colors.Blue600)
        self.backgroundColor = UIColor.init(named: Colors.Gray300)
        
    }
}
