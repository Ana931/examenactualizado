//
//  Snackbar.swift
//  examen-ios
//
//  Created by User on 23/09/22.
//

import UIKit

class Snackbar: UIView {
    
    private lazy var infoLabel: UILabel = {
        let label: UILabel = UILabel()
        return label
    }()
    
    required init (text: String) {
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        addSubViews()
        addConstraints()
    }
    
    private func addSubViews() {
        self.addSubview(infoLabel)
    }
    
    private func addConstraints() {
        addConstrainsForSnackBar()
        addConstrainsForInfoLabel()
    }
    
    private func addConstrainsForSnackBar() {
        self.setHeight(heightConstant: 50)
    }
    
    private func addConstrainsForInfoLabel() {
        infoLabel.setAnchors(topAnchor: self.topAnchor, bottomAnchor: self.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor)
    }
    
}
