//
//  BaseViewController.swift
//  examen-ios
//
//  Created by User on 23/09/22.
//

import UIKit

class BaseViewController: UIViewController {
    lazy private var alert: Snackbar = {
        let alert: Snackbar = Snackbar(text: message)
        return alert
    }()
    private var message: String = ""  {
        didSet {
            alert = Snackbar(text: message)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        commoninit()
        addNavigationBarButton(imageName: Images.iconHamburger, direction: .left)
    }
    
    private func commoninit() {
        self.view.backgroundColor = UIColor.init(named: Colors.BackgroundColor)
        showAlertAddSubViews()
        addConstraintsForShowAlert()
    }
    
    func addNavigationBarButton(imageName: String, direction: direction) {
        var image = UIImage(named: imageName)
        image = image?.withRenderingMode(.alwaysOriginal)
        switch direction {
        case .left:
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(navigateMenu))
        case .right:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(goBack))
        }
    }
    
    @objc func navigateMenu() {
        Router.pushViewController(MenuOptionsRouter.createMenuOptionsViewController(), animated: true)
    }
}

extension BaseViewController {
    
    func showAlert(message: String) {
        self.message = message
    }
    
    private func showAlertAddSubViews() {
        self.view.addSubview(alert)
    }
    
    private func addConstraintsForShowAlert() {
        alert.setAnchors(bottomAnchor: self.view.bottomAnchor, leadingAnchor: self.view.leadingAnchor, trailingAnchor: self.view.trailingAnchor)
    }
}
