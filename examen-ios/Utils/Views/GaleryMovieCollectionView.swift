//
//  GaleryMovieCollectionView.swift
//  examen-ios
//
//  Created by User on 24/10/22.
//

import UIKit

class GaleryMovieCollectionView: UIView {
    var delegate: GaleryMovieCollectionDelegate?
    var results: [MovieModels] = []
    private enum LayoutConstant {
        static let spacing: CGFloat = 8.0
        static let itemHeight: CGFloat = 250.0
    }
    lazy var colletionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.backgroundColor = UIColor.black
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        addSubViews()
        addConstraints()
        configureListMovieColletionView()
    }
    
    private func addSubViews() {
        self.addSubview(colletionView)
    }
    
    private func addConstraints() {
        addConstraintForColletionView()
    }
    
    private func addConstraintForColletionView() {
        colletionView.setAnchors(topAnchor: self.topAnchor, bottomAnchor: self.bottomAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor: self.trailingAnchor)
    }
    
    private func configureListMovieColletionView() {
        self.colletionView.register(MovieLittleDescriptionCell.self, forCellWithReuseIdentifier: "MovieLittleDescriptionCell")
        self.colletionView.dataSource = self
        self.colletionView.delegate = self
        self.colletionView.backgroundColor = UIColor.init(named: Colors.BackgroundColor)
        self.colletionView.alwaysBounceVertical = true
    }
}

extension GaleryMovieCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        delegate?.selectToRow(didSelectItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MovieLittleDescriptionCell = colletionView.dequeueReusableCell(withReuseIdentifier: "MovieLittleDescriptionCell", for: indexPath) as? MovieLittleDescriptionCell else {
            return UICollectionViewCell()
        }
        print("Insert Info collectionView")
        cell.dataContent(with: results[indexPath.row])
        return cell
    }
}

extension GaleryMovieCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = itemWidth(for: self.frame.width, spacing: 0)
        return CGSize(width: width, height: LayoutConstant.itemHeight)
    }

    func itemWidth(for width: CGFloat, spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = 2
        let totalSpacing: CGFloat = 2 * spacing + (itemsInRow - 1) * spacing
        let finalWidth: CGFloat = (width - totalSpacing) / itemsInRow
        return finalWidth - 25.0
    }
}

protocol GaleryMovieCollectionDelegate {
    
    func selectToRow(didSelectItemAt indexPath: IndexPath)
}
