//
//  SelectButtonView.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import UIKit

class SelectButtonView: UIView {
    let label: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = UIColor.white
        return label
    }()
    let button: UIButton = UIButton()
    let separator: UIView = {
        let view: UIView = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    var completion: (() -> Void)?
    required init(title: String, completion: @escaping (() -> Void)) {
        super.init(frame: .zero)
        label.text = title
        self.completion = completion
        commontInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commontInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commontInit() {
        addSubViews()
        addConstraints()
        addTargets()
    }
    
    private func addSubViews() {
        self.addSubview(separator)
        self.addSubview(button)
        self.addSubview(label)
    }
    
    private func addConstraints() {
        addConstraintsForSeparator()
        addContraintsForButton()
        AddContrainsForLabel()
    }
    
    private func addConstraintsForSeparator() {
        separator.setAnchors(topAnchor: self.topAnchor, leadingAnchor: self.leadingAnchor, trailingAnchor:  self.trailingAnchor)
        separator.setHeight(heightConstant: 3)
    }
    
    private func addContraintsForButton() {
        button.setAnchors(topAnchor: self.topAnchor,
                          bottomAnchor: self.bottomAnchor,
                          leadingAnchor: self.leadingAnchor,
                          trailingAnchor: self.trailingAnchor)
        button.setHeight(heightConstant: 50)
    }
    
    private func AddContrainsForLabel() {
        label.setAnchors(topAnchor: self.topAnchor,
                          bottomAnchor: self.bottomAnchor,
                          leadingAnchor: self.leadingAnchor,
                          trailingAnchor: self.trailingAnchor)
    }
    private func addTargets() {
        button.addTarget(self, action: #selector(actionButton), for: .touchDown)
    }
    
    @objc private  func actionButton() {
        guard let closure: (() -> Void) = completion else {
            return
        }
        closure()
    }
}
