//
//  MovieLittleDescriptionCell.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation
import UIKit

class MovieLittleDescriptionCell: UICollectionViewCell {
    
    private lazy var pictureMovie: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.image = UIImage(named: Images.movieTadeo)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    private lazy var titleMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    private lazy var dateMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    private lazy var pointMovieMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    private lazy var descriptionShortMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = UIColor.white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubViews()
        addConstraints()
        configureMovieLittleDescription()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureMovieLittleDescription() {
        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 15)
        self.clipsToBounds = true
        self.backgroundColor = UIColor.init(named: Colors.Gray400)
    }
    
    private func addSubViews() {
        self.contentView.addSubview(pictureMovie)
        self.contentView.addSubview(titleMovieLabel)
        self.contentView.addSubview(dateMovieLabel)
        self.contentView.addSubview(pointMovieMovieLabel)
        self.contentView.addSubview(descriptionShortMovieLabel)
    }
    
    private func addConstraints() {
        addConstraintsForPictureMovie()
        addConstraintsForTitleMovieLabel()
        addConstraintsForDateMovieLabel()
        addConstraintsForPointMovieMovieLabel()
        addConstraintForDescriptionShortMovieLabel()
    }
    
    private func addConstraintsForPictureMovie() {
        pictureMovie.setAnchors(topAnchor: topAnchor, leadingAnchor: leadingAnchor, trailingAnchor: trailingAnchor, leadingConstant: -5, trailingConstant: 5)
        pictureMovie.setHeight(heightConstant: 40)
    }
    
    private func addConstraintsForTitleMovieLabel() {
        titleMovieLabel.setAnchors(topAnchor: pictureMovie.bottomAnchor, leadingAnchor: leadingAnchor, trailingAnchor: trailingAnchor, topConstant: 110)
        titleMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addConstraintsForDateMovieLabel() {
        dateMovieLabel.setAnchors(topAnchor: titleMovieLabel.bottomAnchor, leadingAnchor: leadingAnchor, trailingAnchor: trailingAnchor,topConstant: 2)
        dateMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addConstraintsForPointMovieMovieLabel() {
        pointMovieMovieLabel.setAnchors(topAnchor: dateMovieLabel.bottomAnchor, leadingAnchor: leadingAnchor, trailingAnchor: trailingAnchor)
        pointMovieMovieLabel.setHeight(heightConstant: 20)
    }
    
    private func addConstraintForDescriptionShortMovieLabel() {
        descriptionShortMovieLabel.setAnchors(topAnchor: pointMovieMovieLabel.bottomAnchor, leadingAnchor: leadingAnchor, trailingAnchor: trailingAnchor)
        descriptionShortMovieLabel.setHeight(heightConstant: 20)
    }
    
    func dataContent(with dataContent: MovieModels) {
        titleMovieLabel.text = dataContent.firstAirDate
        dateMovieLabel.text = dataContent.name
        pointMovieMovieLabel.text = "\(dataContent.voteCount)"
        descriptionShortMovieLabel.text = dataContent.overview
    }
}
