//
//  BaseLoginViewController.swift
//  examen-ios
//
//  Created by User on 05/10/22.
//

import UIKit

class BaseLoginViewController: UIViewController {
    lazy private var alert: Snackbar = {
        let alert: Snackbar = Snackbar(text: message)
        alert.isHidden = true
        return alert
    }()
    private var timer: Timer?
    private var message: String = ""  {
        didSet {
            alert = Snackbar(text: message)
        }
    }
    private var timerCurrentCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commoninit()
    }
    
    private func commoninit() {
        self.view.backgroundColor = UIColor.init(named: Colors.BackgroundColor)
        showAlertAddSubViews()
        addConstraintsForShowAlert()
    }
}

extension BaseLoginViewController {
    
    func showAlert(message: String) {
        self.timer = Timer.scheduledTimer(timeInterval: 2,
                                              target: self,
                                              selector: #selector(handleTimerExecution),
                                              userInfo: nil,
                                              repeats: true)
        self.message = message
    }
    
    @objc private func handleTimerExecution() {
        if self.timerCurrentCount == 5 {
            alert.isHidden = true
            self.timer?.invalidate() // invalidating timer
        } else {
            print("timer executed...")
            alert.isHidden = false
            self.timerCurrentCount += 1
        }
    }
    
    private func showAlertAddSubViews() {
        self.view.addSubview(alert)
    }
    
    private func addConstraintsForShowAlert() {
        alert.setAnchors(bottomAnchor: self.view.bottomAnchor, leadingAnchor: self.view.leadingAnchor, trailingAnchor: self.view.trailingAnchor)
        alert.setHeight(heightConstant: 50)
    }
}
